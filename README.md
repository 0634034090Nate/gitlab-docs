# GitLab documentation project

[![build status](https://gitlab.com/gitlab-org/gitlab-docs/badges/main/pipeline.svg)](https://gitlab.com/gitlab-org/gitlab-docs/commits/main)

This project hosts the repository used to generate the GitLab documentation
website deployed to [https://docs.gitlab.com](https://docs.gitlab.com). It
uses the [Nanoc](http://nanoc.ws) static site generator.

## Contribution

If you are interested in contributing improvements to how the <https://docs.gitlab.com> **site** itself works,
see the [GitLab docs site documentation](doc/index.md)

If you are interested in contributing to the **documentation** for specific GitLab products,
you must contribute the documentation changes in the projects for those products:

- [GitLab](https://gitlab.com/gitlab-org/gitlab)
- [Omnibus GitLab](https://gitlab.com/gitlab-org/omnibus-gitlab)
- [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner)
- [GitLab Chart](https://gitlab.com/gitlab-org/charts/gitlab)

If you want to preview your documentation changes and see how they would appear on
<http://docs.gitlab.com>, there are a few ways to [preview GitLab documentation changes](doc/index.md#development-when-contributing-to-git-lab-documentation).

## Contributing agreement

Read [CONTRIBUTING.md](CONTRIBUTING.md) for an overview of the Developer
Certificate of Origin + License.

## License

See [LICENSE](LICENSE).
